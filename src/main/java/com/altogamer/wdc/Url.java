/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

/**
 * General information from an URL.
 */
public class Url {

    public static enum Method {
        GET,
        POST
    }

    private Method method;
    private String url;
    private String payload;
    private String contentType; //content type for payload

    public Url(Method method, String url) {
        this.method = method;
        this.url = url;
    }

    public Url(Method method, String url, String payload, String contentType) {
        this.method = method;
        this.url = url;
        this.payload = payload;
        this.contentType = contentType;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String toString() {
        return "Url{" + "method=" + method + ", url=" + url + ", payload=" + payload + ", contentType=" + contentType + '}';
    }

}
