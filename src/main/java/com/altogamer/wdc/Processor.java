/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

import java.util.List;

/**
 * A processor for items.
 * The implementation of this interface is in charge of processing a group of
 * games.
 */
public interface Processor {

    /**
     * Initializes this Processor to start working.
     * This method is called only once just before the crawling starts.
     */
    default void init() {};

    /**
     * Process a group of items. The items are obtained with a Collector.
     * @param items The items to process.
     * @param url The url that was used to fetch these items.
     */
    void process(List items, Url url);
}
