/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

/**
 * A iterator that returns url to parse.
 */
public interface UrlIterator {

    /**
     * Initializes this UrlIterator to start working.
     * This method is called only once just before the crawling starts.
     */
    default void init() {};

    /**
     * Returns the next URL to process. This Url will be handled by an
     * UrlContentRetriever.
     * @return an Url to process ,or null if there are no more URLs.
     */
    Url next();

    /**
     * Indicates if this UrlIterator returns Urls that may contain empty results
     * when processed by a Collector.
     * This option is used by the WebCrawler, and configures if the crawler
     * should go on crawling URLs even on empty results.
     * Set it to "false" for crawling paged results where this UrlIterator can
     * not know how many pages there are (for instance, when returning paged
     * results with an unknown number of pages).
     * Set it to "true" to allow the crawler to continue processign even when
     * no items are collected (the crawler will only stop when this UrlIterator
     * returns a null Url).
     */
    boolean allowEmptyResults();

}
