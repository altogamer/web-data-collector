/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

import java.util.List;

/**
 * Events from a PageCollector.
 */
public interface PageCollectorListener {

    /**
     * This method is called when a PageCollector finishes its work.
     * @param items The list of items that were collected from the url.
     * @param url the URL that was fetched to collect items.
     */
    void onComplete(List items, Url url);

}
