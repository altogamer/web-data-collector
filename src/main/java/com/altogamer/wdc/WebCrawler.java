/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Web crawler that scans a group of URL using an specific Collector. This class
 * needs an UrlIterator (that returns URLs to parse), a Collector (that collects
 * items from the URLs), and a Processor (that process the items collected).
 */
public class WebCrawler {

    private static final Logger logger = LoggerFactory.getLogger(WebCrawler.class);

    /**
     * The iterator that returns the web pages to parse.
     */
    private UrlIterator urlIterator;
    /**
     * The ContentRetriever that will be used to retrieve content from the
     * UrlIterator.
     */
    private ContentRetriever contentRetriever;
    /**
     * The collector for parsing web pages.
     */
    private Collector collector;
    /**
     * The Processor that will process the items parsed.
     */
    private Processor processor;

    /* Default Thead Pool Executor values. */
    private int corePoolSize = 15;
    private int maximumPoolSize = 15;
    private int keepAliveTime = 3600;
    private TimeUnit keepAliveTimeUnit = TimeUnit.SECONDS;

    /**
     * A custom property that describes this crawler. This value is not used
     * by the crawler, and its only useful to the client.
     */
    private Map<String, Object> customProperty;

    /**
     * Counter that stores collected items count.
     */
    private int itemsCount;

    /**
     * Collects all items from a website. This method spawns multiple threads to
     * collect items from different URLs, using the Collector to extract items
     * from each web page.
     */
    public void run() throws IOException, InterruptedException {
        itemsCount = 0;
        long startTime = System.currentTimeMillis();
        BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
        final ThreadPoolExecutor pool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, keepAliveTimeUnit, queue);

        urlIterator.init();
        contentRetriever.init();
        collector.init();
        processor.init();

        PageCollectorListener pageCollectorListener = new PageCollectorListener() {
            @Override
            public void onComplete(List itemsCollected, Url url) {
                processor.process(itemsCollected, url);
                logItemsDetails(itemsCollected, url);
                incrementItemsCount(itemsCollected.size());
                //check if there may be more items and schedule next url
                Url nextUrl = urlIterator.next();
                if (nextUrl != null && (urlIterator.allowEmptyResults() || itemsCollected.size() > 0)) {
                    PageCollectorTask pageCollector = new PageCollectorTask(nextUrl, contentRetriever, collector, this);
                    pool.execute(pageCollector);
                }
            }
        };

        //fill pool with initial executors
        for (int i = 0; i < maximumPoolSize; i++) {
            PageCollectorTask pageCollector = new PageCollectorTask(urlIterator.next(), contentRetriever, collector, pageCollectorListener);
            pool.execute(pageCollector);
        }

        awaitCompletion(pool);
        pool.shutdown();

        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        logger.info("Finished. {} items collected in {}ms.", itemsCount, totalTime);
    }

    /**
     * Logs a collection of items.
     */
    private void logItemsDetails(List items, Url url) {
        if (logger.isDebugEnabled()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Collected ").append(items.size()).append(" items from ").append(url).append("\n");
            for (Object item : items) {
                sb.append(item.toString());
                sb.append("\n");
            }
            logger.debug(sb.toString());
        } else {
            logger.info("Collected {} items from {}", items.size(), url);
        }
    }

    /**
     * Blocks until all tasks have completed execution.
     */
    private void awaitCompletion(ThreadPoolExecutor pool) throws InterruptedException {
        while (pool.getTaskCount() != pool.getCompletedTaskCount()) {
            logger.info("Altogamer Web Data Collector is running... Task count: {} - Completed: {}", pool.getTaskCount(), pool.getCompletedTaskCount());
            Thread.sleep(10000);
        }
    }

    private synchronized void incrementItemsCount(int count) {
        itemsCount += count;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public TimeUnit getKeepAliveTimeUnit() {
        return keepAliveTimeUnit;
    }

    public void setKeepAliveTimeUnit(TimeUnit keepAliveTimeUnit) {
        this.keepAliveTimeUnit = keepAliveTimeUnit;
    }

    public Collector getCollector() {
        return collector;
    }

    public void setCollector(Collector collector) {
        this.collector = collector;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public UrlIterator getUrlIterator() {
        return urlIterator;
    }

    public void setUrlIterator(UrlIterator urlIterator) {
        this.urlIterator = urlIterator;
    }

    public ContentRetriever getContentRetriever() {
        return contentRetriever;
    }

    public void setContentRetriever(ContentRetriever contentRetriever) {
        this.contentRetriever = contentRetriever;
    }

    public void addCustomProperty(String name, Object value) {
        if (customProperty == null) {
            customProperty = new HashMap<>();
        }
        customProperty.put(name, value);
    }

    public Object getCustomProperty(String name) {
        if (customProperty == null) {
            return null;
        }
        return customProperty.get(name);
    }

}
