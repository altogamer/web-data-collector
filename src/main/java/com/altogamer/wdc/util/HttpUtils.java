/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.util;

import com.altogamer.wdc.Url;
import java.io.IOException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

/**
 * General utilities for handling HTML.
 */
public class HttpUtils {

    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0";

    /**
     * Returns the content of a URL as String.
     *
     * @param url the URL to fetch.
     * @return the content of the URL as String.
     */
    public static String getBodyFromUrl(String url) throws IOException {
        HttpResponse result = HttpRequest.get(url)
                .header("User-Agent", USER_AGENT)
                .send();
        return result.statusCode() == 200 ? result.bodyText() : "";
    }

    public static String getBodyFromUrl(Url url) {
        HttpRequest request;
        switch (url.getMethod()) {
            case GET: request = HttpRequest.get(url.getUrl());
               break;
            case POST:
                request = HttpRequest.post(url.getUrl());
                break;
            default:
                throw new UnsupportedOperationException("Method not supported: " + url.getMethod());
        }
        
        request.header("User-Agent", USER_AGENT);
        if (url.getPayload() != null) {
            request.bodyText(url.getPayload(), url.getContentType());
        }

        HttpResponse result = request.send();
        return result.statusCode() == 200 ? result.bodyText() : "";
    }
}
