/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A process unit to collect items from an specific URL using an specific Collector.
 * This class is designed to be run as a thread.
 */
public class PageCollectorTask implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(PageCollectorTask.class);

    private Url url;
    private Collector collector;
    private PageCollectorListener listener;
    private ContentRetriever urlContentRetriever;

    public PageCollectorTask(Url url, ContentRetriever urlContentRetriever, Collector collector, PageCollectorListener listener) {
        this.url = url;
        this.urlContentRetriever = urlContentRetriever;
        this.collector = collector;
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            if (url != null) {
                String content = urlContentRetriever.getContent(url);
                List items = collector.collect(content);
                listener.onComplete(items, url);
            }
        } catch (Exception ex) {
            logger.error("Error processing url, completing with empty results: " + url.getUrl(), ex);
            listener.onComplete(new ArrayList(), url);
        }
    }

}
