/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

import java.io.IOException;
import java.util.List;

/**
 * Extracts and returns item information from a given content.
 */
public interface Collector {

    /**
     * Initializes this Collector to start working. This method is called only
     * once just before the crawling starts.
     */
    default void init() {};

    /**
     * Parses the given content and returns the items found. Can return any kind
     * of object. This List will be processed by a Processor.
     */
    List collect(String content) throws IOException;

}
