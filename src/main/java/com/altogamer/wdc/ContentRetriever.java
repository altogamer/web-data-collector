/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc;

import java.io.IOException;

/**
 * Retrieves content from an URL. The ContentRetriever is given an URL
 (obtained via UrlIterator), and is in charge of retrieving its content.
 The content will then be processed by a Collector.
 */
public interface ContentRetriever {

    /**
     * Initializes this ContentRetriever to start working.
     * This method is called only once just before the crawling starts.
     */
    default void init() {};

    /**
     * Gets the content from the given URL.
     */
    String getContent(Url url) throws IOException;
}
