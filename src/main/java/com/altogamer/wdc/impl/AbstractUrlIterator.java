/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.impl;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;

/**
 * An abstract implementation of UrlIterator that enables easiers implementations.
 */
public abstract class AbstractUrlIterator implements UrlIterator {

    protected int limit = -1;
    protected int index = 0;

    public AbstractUrlIterator() {
        init();
    }

    /**
     * Returns the URL to increment. The URL String must contain the placeholder
     * {index} to put the index. For example: "http://www.pcgamer.com/category/reviews/page/{index}/"
     * @return a new base URL to use.
     */
    public abstract Url createBaseUrl();

    /**
     * The count that will be added to the index each iteration.
     * @return the index increment.
     */
    public abstract int getIndexIncrement();

    @Override
    public void init() {
        index = 0;
    }

    @Override
    public synchronized Url next() {
        if (limit == 0 || (limit > -1 && index >= limit * getIndexIncrement())) {
            return null;
        }
        Url baseUrl = createBaseUrl();
        baseUrl.setUrl(baseUrl.getUrl().replaceAll("\\{index\\}", String.valueOf(index)));
        index += getIndexIncrement();
        return baseUrl;
    }

    @Override
    public boolean allowEmptyResults() {
        return false;
    }

    /**
     * Limits the URL count to process. If the limit is reached, and invalid
     * URL will be returned instead. -1 to disable the limit.
     * @param limit the URL count limit. Defaults to -1 (no limit).
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

}
