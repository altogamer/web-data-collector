/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.impl;

import com.altogamer.wdc.Processor;
import com.altogamer.wdc.Url;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple Processor that collects all items in an List.
 */
public class SimpleProcessor implements Processor {

    private List items = new ArrayList<>();

    @Override
    public void process(List collectedItems, Url url) {
        items.addAll(collectedItems);
    }

    public List getDataItems() {
        return items;
    }

}
