/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.impl;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.util.HttpUtils;
import java.io.IOException;

/**
 * A simple implementation for ContentRetriever that just retrieves the
 content from the URL.
 */
public class SimpleUrlContentRetriever implements ContentRetriever {

    @Override
    public String getContent(Url url) throws IOException {
        switch (url.getMethod()) {
            case GET:
            case POST:
                return HttpUtils.getBodyFromUrl(url);
            default:
                throw new UnsupportedOperationException("Not implemented yet: " + url.getMethod());
        }
    }

}
