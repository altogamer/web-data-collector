/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.examples;

import com.altogamer.wdc.WebCrawler;
import com.altogamer.wdc.impl.SimpleProcessor;
import com.altogamer.wdc.examples.metacritic.MetacriticCollector;
import com.altogamer.wdc.examples.metacritic.MetacriticUrlIterator;
import com.altogamer.wdc.impl.SimpleUrlContentRetriever;
import java.io.IOException;

/**
 * Example for running a WebCrawler quickly. Useful for quick testing and benchmarking.
 *
 * Benchmarks: CorePoolSize (CPS) / MaximumPoolSize (MPS)
 *     CPS=10, MPS=10 : 9592 games collected in 85021ms
 *     CPS=15, MPS=15 : 9592 games collected in 85023ms
 *     CPS=20, MPS=20 : 9592 games collected in 90023ms
 *     CPS=25, MPS=25 : 9592 games collected in 90023ms
 *     CPS=30, MPS=30 : 9592 games collected in 100026ms
 *     CPS=40, MPS=40 : 9592 games collected in 125027ms
 */
public class App {

    public static void main(String[] args) throws IOException, InterruptedException {
        WebCrawler crawler = new WebCrawler();
        crawler.setUrlIterator(new MetacriticUrlIterator());
        crawler.setContentRetriever(new SimpleUrlContentRetriever());
        crawler.setCollector(new MetacriticCollector());
        crawler.setProcessor(new SimpleProcessor());

        crawler.setCorePoolSize(30);
        crawler.setMaximumPoolSize(30);

        crawler.run();
    }
}
