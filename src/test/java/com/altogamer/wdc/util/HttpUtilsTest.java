/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.util;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author ldeseta
 */
public class HttpUtilsTest {

    @Test
    public void getBodyFromUrl_validUrl_returnsBodyText() throws Exception {
        String url = "http://www.gamesradar.com/pc/reviews/list/?order_by=newest-first&page=1";
        String result = HttpUtils.getBodyFromUrl(url);
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    public void getBodyFromUrl_urlRedirects_returnsEmptyString() throws Exception {
        String url = "http://www.gamesradar.com/pc/reviews/list/?order_by=newest-first&page=9999";
        String result = HttpUtils.getBodyFromUrl(url);
        assertEquals("", result);
    }
}