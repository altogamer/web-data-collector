/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.walmart;

import com.altogamer.wdc.WebCrawler;
import com.altogamer.wdc.impl.SimpleProcessor;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

public class WalmartWebCrawlerTest {

    @Test
    public void run_defaultSettings_returnsGames() throws Exception {
        SimpleProcessor processor = new SimpleProcessor();
        WalmartUrlIterator urlIterator = new WalmartUrlIterator();

        WebCrawler crawler = new WebCrawler();
        crawler.setCollector(new WalmartCollector());
        crawler.setProcessor(processor);
        crawler.setUrlIterator(urlIterator);
        crawler.setContentRetriever(new WalmartUrlContentRetriever());

        crawler.setCorePoolSize(5);
        crawler.setMaximumPoolSize(5);

        crawler.run();

        assertNotNull(processor.getDataItems());
        assertTrue(processor.getDataItems().size() > 0);
    }

}