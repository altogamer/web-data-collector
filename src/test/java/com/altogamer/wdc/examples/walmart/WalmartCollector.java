/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.walmart;

import com.altogamer.wdc.Collector;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WalmartCollector implements Collector {

    @Override
    public List<Map> collect(String content) throws IOException {
        System.out.println("=======================================================================");
        System.out.println(content);
        System.out.println("=======================================================================");
        return new ArrayList<>();
    }

}
