/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.walmart;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.util.HttpUtils;
import java.io.IOException;
import jodd.http.HttpBrowser;
import jodd.http.HttpRequest;

public class WalmartUrlContentRetriever implements ContentRetriever {

    @Override
    public String getContent(Url url) throws IOException {
        HttpRequest req = new HttpRequest();
        req.header("User-Agent", HttpUtils.USER_AGENT);

        HttpBrowser browser = new HttpBrowser();
        sendUrl(browser, req, url.getUrl());
        sendUrl(browser, req, "https://walmartonline.com.ar/WebControls/hlSearchProducts.ashx");
        return browser.getHttpResponse().bodyText();
    }

    private void sendUrl(HttpBrowser browser, HttpRequest req, String url) {
        req.set(url);
        browser.sendRequest(req);
    }

}
