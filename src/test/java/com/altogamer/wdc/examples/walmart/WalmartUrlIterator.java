/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.walmart;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;

public class WalmartUrlIterator implements UrlIterator {

    private static final String BASE_URL = "https://walmartonline.com.ar/WebControls/hlSearchResults.ashx?busqueda=undefined&departamento={departamento}&familia=undefined&linea=undefined&orderby=undefined&orderbyid=undefined&range=undefined&sid=2.6403622487023384";
    private static final String[] DEPARTAMENTOS = new String[]{
        "D_almacen",
        "D_bebidas%20con%20alcohol",
        "D_bebidas%20sin%20alcohol",
        "D_carniceria",
        "D_congelados",
        "D_farmacia",
        "D_fiambreria",
        "D_frutas%20y%20verduras",
        "D_golosinas%20y%20galletitas",
        "D_lacteos",
        "D_limpieza",
        "D_panaderia",
        "D_pastas%20y%20tapas",
        "D_perfumeria"
    };

    private int index = 0;

    @Override
    public void init() {
        index = 0;
    }

    @Override
    public Url next() {
        if (index >= DEPARTAMENTOS.length) {
            return null;
        }
        return new Url(Url.Method.GET, BASE_URL.replaceAll("\\{departamento\\}", DEPARTAMENTOS[index++]));
    }

    @Override
    public boolean allowEmptyResults() {
        return true;
    }

}
