/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.gog;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at GOG (Good Old Games).
 */
public class GogUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Method.GET, "http://www.gog.com/games/ajax/filtered?mediaType=game&sort=bestselling&system=windows_7,windows_8,windows_vista,windows_xp&page={index}");
    }

    @Override
    public void init() {
        index = 1;
    }

    @Override
    public int getIndexIncrement() {
        return 1;
    }

}
