/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.gog;

import com.altogamer.wdc.Collector;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Scans and collects game information from an specific game list page from GOG
 * (Good Old Games).
 */
public class GogCollector implements Collector {

    private static final String URL_PREFIX = "http://www.gog.com";

    /**
     * Returns the names, prices and urls from the game list in the page.
     *
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List collect(String json) throws IOException {
        final List results = new ArrayList<>();
        if (json.isEmpty()) {
            return results;
        }

        ObjectMapper mapper = new ObjectMapper();
        Iterator<JsonNode> elements = mapper.readTree(json).get("products").elements();

        while (elements.hasNext()) {
            JsonNode element = elements.next();
            if (!element.get("isGame").asBoolean()) {
                continue;
            }
            Map<String, String> item = new HashMap<>();

            item.put("name", prepareName(element.get("title").asText()));
            if (isInvalidGame(item.get("name"))) {
                continue;
            }
            item.put("price", getPrice(element.get("price")));
            item.put("url", URL_PREFIX + element.get("url").asText());
            item.put("score", element.get("rating").asText());

            results.add(item);
        }

        return results;
    }

    private String prepareName(String name) {
        name = name.trim();
        name = removeSpecialChars(name);
        if (name.endsWith(", The")) {
            name = name.substring(0, name.lastIndexOf(", The"));
            name = "The " + name;
        }
        return name;
    }

    private String removeSpecialChars(String str) {
        return str.replaceAll("[®™]", "");
    }

    private String getPrice(JsonNode price) {
        if (price.get("isFree").asBoolean()) {
            return "0";
        }
        else {
            return price.get("amount").asText();
        }
    }

    private boolean isInvalidGame(String name) {
        return name.startsWith("DLC: ");
    }

}
