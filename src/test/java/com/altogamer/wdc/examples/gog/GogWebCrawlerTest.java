/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.gog;

import com.altogamer.wdc.WebCrawler;
import com.altogamer.wdc.impl.SimpleProcessor;
import com.altogamer.wdc.impl.SimpleUrlContentRetriever;
import java.util.Map;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

public class GogWebCrawlerTest {

    @Test
    @Ignore
    public void run_defaultSettings_returnsGames() throws Exception {
        SimpleProcessor processor = new SimpleProcessor();
        GogUrlIterator urlIterator = new GogUrlIterator();
        urlIterator.setLimit(5);

        WebCrawler crawler = new WebCrawler();
        crawler.setCollector(new GogCollector());
        crawler.setProcessor(processor);
        crawler.setUrlIterator(urlIterator);
        crawler.setContentRetriever(new SimpleUrlContentRetriever());

        crawler.setCorePoolSize(5);
        crawler.setMaximumPoolSize(5);

        crawler.run();

        assertNotNull(processor.getDataItems());
        assertTrue(processor.getDataItems().size() > 0);

        for (Object gameScore : processor.getDataItems()) {
            Map game = (Map) gameScore;
            System.out.println(game.get("name") + ", " + game.get("score"));
        }
    }

}
