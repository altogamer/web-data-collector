/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.gog;

import com.altogamer.wdc.Url;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class GogUrlIteratorTest {

    private GogUrlIterator iterator;

    @Before
    public void setup() throws IOException {
        iterator = new GogUrlIterator();
    }

    @Test
    public void setLimit_limitDefault_returnsManyUrl() {
        for (int i = 1; i < 20; i += 1) {
            Url url = iterator.next();
            assertTrue(url.getUrl().endsWith(String.valueOf(i)));
        }
    }

    @Test
    public void setLimit_zeroPages_returnsInvalidUrl() {
        iterator.setLimit(0);
        Url url = iterator.next();
        assertTrue(url.getUrl().endsWith("9999"));
    }

    @Test
    public void setLimit_somePages_triggersLimit() {
        iterator.setLimit(4);
        Url url;
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("1"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("2"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("3"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("9999"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("9999"));
    }

}
