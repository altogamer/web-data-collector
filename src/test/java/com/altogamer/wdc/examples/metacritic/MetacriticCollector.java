/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.metacritic;

import com.altogamer.wdc.Collector;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;
import jodd.util.StringUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Scans and collects game information from an specific game list page from
 * Metacritic.
 */
public class MetacriticCollector implements Collector {

    private static final String URL_PREFIX = "http://www.metacritic.com";
    private static final DateTimeFormatter PUBLISHED_DATE_FORMATTER = DateTimeFormat.forPattern("MMM d, YYYY").withLocale(Locale.ENGLISH);

    /**
     * Returns all the game information from the game list in the page.
     *
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List collect(String html) throws IOException {
        Jerry doc = jerry(html);
        final List<Map> results = new ArrayList<>();
        doc.$("div#main ol.list_products.list_product_summaries li.product div.product_wrap").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                Map<String, Object> item = new HashMap<>();

                Jerry nameNode = $this.find(".product_title a");
                item.put("name", nameNode.text().trim());
                item.put("url", URL_PREFIX + nameNode.attr("href"));

                Jerry scoreNode = $this.find(".product_score span");
                item.put("score", scoreNode.text());

                Jerry releaseDateNode = $this.find("li.release_date span.data");
                item.put("releaseDate", stringToDate(releaseDateNode.text()).toString());

                Jerry publisherNode = $this.find("li.publisher span.data");
                item.put("publisher", defaultIfBlank(publisherNode.text(), null));

                results.add(item);
                return true;
            }
        });
        return results;
    }

    private Date stringToDate(String s) {
        //some dates have two-spaces in between, remove them.
        s = s.trim().replaceAll("\\s+", " ");
        if (s.isEmpty()) {
            return null;
        } else {
            return DateTime.parse(s, PUBLISHED_DATE_FORMATTER).toDate();
        }
    }

    private String defaultIfBlank(String s, String defaultStr) {
        if (StringUtil.isBlank(s)) {
            return defaultStr;
        }
        return s;
    }

}
