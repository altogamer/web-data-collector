/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.metacritic;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at Metacritic.
 */
public class MetacriticUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Method.GET, "http://www.metacritic.com/browse/games/release-date/available/pc/name?view=detailed&page={index}");
    }

    @Override
    public int getIndexIncrement() {
        return 1;
    }

}
