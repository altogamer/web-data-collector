/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.metacritic;

import com.altogamer.wdc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.*;
import org.junit.Test;

public class MetacriticCollectorTest {

    private String getHtmlWithGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.metacritic.com/browse/games/release-date/available/pc/name?view=detailed&page=0");
    }

    private String getHtmlWithEmptyGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.metacritic.com/browse/games/release-date/available/pc/name?view=detailed&page=9999");
    }

    private boolean isValidScore(Object score) {
        if (score.equals("tbd")) {
            return true;
        }
        try {
            Integer.parseInt(score.toString());
            return true;
        }
        catch (NumberFormatException ex) {
            return false;
        }
    }

    @Test
    public void collect_pageWithGames_returnsGames() throws Exception {
        MetacriticCollector instance = new MetacriticCollector();
        List<Map> results = instance.collect(getHtmlWithGameList());
        assertNotNull(results);
        assertEquals(100, results.size());
        for (Map gameScore : results) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.get("name"));
            assertNotNull(gameScore.get("url"));
            assertNotNull(gameScore.get("releaseDate"));
            assertNotNull(gameScore.get("score"));
            //assertTrue(StringUtil.isNotBlank(gameScore.getPublisher()));
            assertTrue(isValidScore(gameScore.get("score")));
        }
    }

    @Test
    public void collect_pageWithNoGames_returnsEmptyList() throws Exception {
        MetacriticCollector instance = new MetacriticCollector();
        List<Map> results = instance.collect(getHtmlWithEmptyGameList());
        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

}