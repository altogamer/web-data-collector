/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.wdc.examples.metacritic;

import com.altogamer.wdc.WebCrawler;
import com.altogamer.wdc.impl.SimpleProcessor;
import com.altogamer.wdc.impl.SimpleUrlContentRetriever;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

public class MetacriticWebCrawlerTest {

    @Test
    @Ignore
    public void run_defaultSettings_returnsGames() throws Exception {
        SimpleProcessor processor = new SimpleProcessor();
        MetacriticUrlIterator urlIterator = new MetacriticUrlIterator();
//        urlIterator.setLimit(5);

        WebCrawler crawler = new WebCrawler();
        crawler.setCollector(new MetacriticCollector());
        crawler.setProcessor(processor);
        crawler.setUrlIterator(urlIterator);
        crawler.setContentRetriever(new SimpleUrlContentRetriever());

        crawler.setCorePoolSize(5);
        crawler.setMaximumPoolSize(5);

        crawler.run();

        assertNotNull(processor.getDataItems());
        assertTrue(processor.getDataItems().size() > 0);
    }

}