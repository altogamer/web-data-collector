# Alto Gamer Web Data Collector
##### An extensible web data collector framework

## What is Alto Gamer Web Data Collector?
Alto Gamer Web Data Collector is a Java web crawler that collects data
from given URLs. It also is very extensible, and supports custom parsers for
any URL.

## License
Alto Gamer Web Data Collector is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at Mozilla Public License FAQ.